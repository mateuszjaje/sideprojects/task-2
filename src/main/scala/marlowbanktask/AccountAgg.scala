package marlowbanktask

import cats.syntax.either._
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

case class AccountAgg(
    id: AccountId,
    balance: Money,
) {
  def canWithdraw(amount: Money): Boolean = balance.amount >= amount.amount

  def applyEvent(event: AccountEvent) = event match {
    case Deposit(_, amount)  => copy(balance = Money(balance.amount + amount.amount))
    case Withdraw(_, amount) => copy(balance = Money(balance.amount - amount.amount))
  }

  def withdraw(amount: Money): Either[DomainErrors, Withdraw] =
    if (canWithdraw(amount)) Right(Withdraw(id, amount))
    else Left(CantWithdraw("balance too low"))

  def deposit(amount: Money): Either[DomainErrors, Deposit] =
    Deposit(id, amount).asRight[DomainErrors]

}

object AccountAgg {
  implicit val codec: Codec[AccountAgg] = deriveCodec
}
