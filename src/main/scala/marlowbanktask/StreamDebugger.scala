package marlowbanktask

import org.apache.kafka.streams.scala.kstream.KStream

object StreamDebugger {
  implicit class Debugger[K, V](s: KStream[K, V]) {
    def debug(hint: String) = {
      s.map { (k, v) =>
        println(s"$hint: $k => $v")
        (k, v)
      }
    }

  }

}
