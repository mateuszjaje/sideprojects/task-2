package marlowbanktask

import cats.syntax.either._
import com.goyeau.kafka.streams.circe.CirceSerdes._
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import marlowbanktask.RunnerCommon.props
import marlowbanktask.StreamDebugger.Debugger
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse
import org.apache.kafka.streams.kstream.GlobalKTable
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala._
import org.apache.kafka.streams.scala.kstream._

case class AcceptedEvent(
    requestId: String,
    event: AccountEventKafka,
)

object AcceptedEvent {
  val tupled: ((String, AccountEventKafka)) => AcceptedEvent = (apply _).tupled
  implicit val codec: Codec[AcceptedEvent]                   = deriveCodec
}

object Topics {
  val Requests       = "requests"
  val Responses      = "responses"
  val Notifications  = "notifications"
  val AcceptedEvents = "accepted-events"
  val Events         = "events-store"
  val ReadModel      = "read-model"
}

object MainApp {
  import MySerdes.accountIdSerde

  def createMainAppTopology(builder: StreamsBuilder) = {
    val readModel: GlobalKTable[AccountId, AccountAgg] = builder.globalTable[AccountId, AccountAgg](Topics.ReadModel)
    attachMainAppTopology(readModel, builder)
  }

  def attachMainAppTopology(readModel: GlobalKTable[AccountId, AccountAgg], builder: StreamsBuilder) = {
    import KafkaExt._

    def parseRequest(request: Request) = {
      request match {
        case Request(rID, "deposit", Some(amount))  => Right(RequestDeposit(rID, Money(amount)))
        case Request(rId, "deposit", _)             => Left(Response(rId, "missing amount"))
        case Request(rId, "withdraw", Some(amount)) => Right(RequestWithdraw(rId, Money(amount)))
        case Request(rId, "withdraw", _)            => Left(Response(rId, "missing amount"))
        case Request(rId, "balance", _)             => Right(RequestBalance(rId))
        case another                                => Left(Response(another.requestId, s"unknown case $another"))
      }
    }

    def reportParsingFailures(stream: KStream[AccountId, Either[Response, ParsedRequest]]) = stream
      .flatMapValues(e => e.left.toOption)
      .to(Topics.Responses)

    def reportDomainErrors(stream: KStream[AccountId, (String, Either[DomainErrors, Either[Response, AccountEvent]])]) = stream
      .flatMapValues(e => e._2.left.toOption.map(e._1 -> _))
      .mapValues((_, data) => Response(data._1, data._2.toString))
      .to(Topics.Responses)

    def passThroughAcceptedEvents(stream: KStream[AccountId, (String, Either[DomainErrors, Either[Response, AccountEvent]])]) = {
      stream
        .flatMapValues(v => v._2.toList.flatMap(_.toList).map(_.toKafkaModel).map(v._1 -> _).map(AcceptedEvent.tupled))
        .debug("passThroughToAcceptedEvents")
        .to(Topics.AcceptedEvents)
    }

    def answer(stream: KStream[AccountId, (String, Either[DomainErrors, Either[Response, AccountEvent]])]) = {
      stream
        .flatMapValues(v => v._2.toList.flatMap(_.left.toSeq))
        .debug("answer")
        .to(Topics.Responses)
    }

    def progressParsed(stream: KStream[AccountId, Either[Response, ParsedRequest]]) = {
      val value: KStream[AccountId, (String, Either[DomainErrors, Either[Response, AccountEvent]])] = stream
        .flatMap((id, e) => e.toList.map(id -> (id, _)))
        .leftJoin(readModel)(
          (id, _) => id,
          (e, agg) => {
            val updatedAgg = Option(agg).getOrElse(AccountAgg(e._1, Money(0)))
            e._2 match {
              case w: RequestWithdraw =>
                e._2.requestId -> updatedAgg.withdraw(w.amount).map(_.asRight)
              case d: RequestDeposit =>
                e._2.requestId -> updatedAgg.deposit(d.amount).map(_.asRight)
              case _: RequestBalance =>
                // actually requesting balance is not optimal here, it should be routed to another topic, to decouple it from balance altering methods,
                // such a topic could be randomly distributed to consumers, whilst balance operations are partitioned by accountID
                // to give a real concurrent access here, but for this task i stop on mentioning it
                e._2.requestId -> Response(e._2.requestId, s"Your balance is ${updatedAgg.balance}").asLeft.asRight
            }
          },
        )
      value
        .split()
        .branch((_, v) => v._2.isLeft, Branched.withConsumer(reportDomainErrors, "domain-logic-failures"))
        .branch((_, v) => v._2.exists(_.isRight), Branched.withConsumer(passThroughAcceptedEvents, "account-events-stream"))
        .branch((_, v) => v._2.exists(_.isLeft), Branched.withConsumer(answer, "responses-stream"))
    }

    builder
      .stream[AccountId, Request](Topics.Requests)
      .map((id, req) => id -> parseRequest(req))
      .split()
      .branch((_, v) => v.isLeft, Branched.withConsumer(reportParsingFailures, "request-parsing-failures"))
      .branch((_, v) => v.isRight, Branched.withConsumer(progressParsed, "request-progressed"))

    // reply success
    builder
      .stream[AccountId, AcceptedEvent](Topics.AcceptedEvents)
      .mapValues(x => Response(x.requestId, s"event accepted: ${x.event}"))
      .to(Topics.Responses)

    // publish to events topic (and store to events store)
    builder
      .stream[AccountId, AcceptedEvent](Topics.AcceptedEvents)
      .mapValues(_.event)
      .to(Topics.Events)

    // read model updater
    builder
      .stream[AccountId, AccountEventKafka](Topics.Events)
      .leftJoin(readModel)(
        (id, _) => id,
        (evt, agg) => {
          val uAgg = Option(agg).getOrElse(AccountAgg(evt.toDomainModel.id, Money(0)))
          uAgg.applyEvent(evt.toDomainModel)
        },
      )
      .to(Topics.ReadModel)

    // side effecting
    builder
      .stream[AccountId, AccountEventKafka](Topics.Events)
      .foreach((_, v) => println(v))

  }

  def main(args: Array[String]): Unit = {
    val settings = props(args.head, "theapp")

    val builder = new StreamsBuilder()
    createMainAppTopology(builder)

    val topology = builder.build()

    val streams: KafkaStreams = new KafkaStreams(topology, settings)

    streams.setUncaughtExceptionHandler(new StreamsUncaughtExceptionHandler {
      override def handle(exception: Throwable) = {
        exception.printStackTrace()
        StreamThreadExceptionResponse.REPLACE_THREAD
      }
    })

    streams.start()

    sys.ShutdownHookThread.apply(streams.close())
  }

}
