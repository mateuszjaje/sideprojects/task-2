package marlowbanktask

import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.scala.serialization.Serdes

import java.util.Properties

object RunnerCommon {
  def props(kafkaUrl: String, appName: String): Properties = {
    val p = new Properties()
    p.put(StreamsConfig.APPLICATION_ID_CONFIG, appName)
    p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaUrl)
    p.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.stringSerde.getClass)
    p
  }

}
