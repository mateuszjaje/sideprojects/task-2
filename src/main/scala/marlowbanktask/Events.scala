package marlowbanktask

import io.circe.{Decoder, Encoder}

case class AccountId(id: String) extends AnyVal
object AccountId {
  implicit val decoder: Decoder[AccountId] = Decoder.decodeString.map(AccountId(_))
  implicit val encoder: Encoder[AccountId] = Encoder.encodeString.contramap[AccountId](_.id)
}

case class Money(amount: Long) extends AnyVal
object Money {
  implicit val decoder: Decoder[Money] = Decoder.decodeLong.map(Money(_))
  implicit val encoder: Encoder[Money] = Encoder.encodeLong.contramap[Money](_.amount)
}

trait AccountEvent extends Product with Serializable {
  def id: AccountId
}

object AccountEvent

case class Deposit(id: AccountId, amount: Money)  extends AccountEvent
case class Withdraw(id: AccountId, amount: Money) extends AccountEvent

trait DomainErrors
case class CantWithdraw(reason: String) extends DomainErrors
