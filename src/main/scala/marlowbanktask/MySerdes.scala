package marlowbanktask

import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serdes.StringSerde

object MySerdes {
  implicit val accountIdSerde: Serde[AccountId] = {
    val ssd = new StringSerde

    new Serde[AccountId] {
      override def serializer() = (topic: String, data: AccountId) => ssd.serializer().serialize(topic, data.id)

      override def deserializer() = (topic: String, data: Array[Byte]) => AccountId(ssd.deserializer().deserialize(topic, data))
    }
  }

}
