package marlowbanktask

import com.goyeau.kafka.streams.circe.CirceSerdes._
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import marlowbanktask.MySerdes.accountIdSerde
import marlowbanktask.RunnerCommon.props
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse
import org.apache.kafka.streams.kstream.GlobalKTable
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala._

object Notifier {
  val `$100` = 10000

  def createNotifierTopology(builder: StreamsBuilder): Unit = {
    val readModel: GlobalKTable[AccountId, AccountAgg] = builder.globalTable[AccountId, AccountAgg](Topics.ReadModel)
    attachNotifierTopology(readModel, builder)
  }

  def attachNotifierTopology(readModel: GlobalKTable[AccountId, AccountAgg], builder: StreamsBuilder): Unit = {

    builder
      .stream[AccountId, AccountEventKafka](Topics.Events)
      .leftJoin(readModel)(
        (id, _) => id,
        (evt, agg) => {
          evt match {
            case WithdrawAccountEventKafka(accountId, amount) =>
              Option(agg).map(_.balance).flatMap { currentBalance =>
                if (currentBalance.amount > `$100` && (currentBalance.amount - amount.amount) < `$100`) {
                  Some(LowBalanceNotification(accountId, Money(currentBalance.amount - amount.amount)))
                } else None
              }
            case _ => None
          }
        },
      )
      .flatMapValues(_.toList)
      .to(Topics.Notifications)

    builder
      .stream[AccountId, LowBalanceNotification](Topics.Notifications)
      .foreach((_, notification) => println(s"========= $notification"))
  }

  def main(args: Array[String]): Unit = {
    val settings = props(args.head, "notifier")
    val builder  = new StreamsBuilder()
    createNotifierTopology(builder)
    val topology = builder.build()

    val streams: KafkaStreams = new KafkaStreams(topology, settings)

    streams.setUncaughtExceptionHandler(new StreamsUncaughtExceptionHandler {
      override def handle(exception: Throwable) = {
        exception.printStackTrace()
        StreamThreadExceptionResponse.REPLACE_THREAD
      }
    })

    streams.start()

    sys.ShutdownHookThread.apply(streams.close())
  }

}

case class LowBalanceNotification(acc: AccountId, currentBalance: Money)
object LowBalanceNotification {
  implicit val codec: Codec[LowBalanceNotification] = deriveCodec
}
