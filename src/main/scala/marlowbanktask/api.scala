package marlowbanktask

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

case class Request(
    requestId: String,
    kind: String,
    amount: Option[Long],
)

object Request {
  implicit val codec: Codec[Request] = deriveCodec
}

case class Response(
    requestId: String,
    response: String,
)

object Response {
  implicit val codec: Codec[Response] = deriveCodec
}

trait ParsedRequest extends Product with Serializable {
  def requestId: String
}

case class RequestWithdraw(
    requestId: String,
    amount: Money,
) extends ParsedRequest

object RequestWithdraw {
  implicit val codec: Codec[RequestWithdraw] = deriveCodec
}

case class RequestDeposit(
    requestId: String,
    amount: Money,
) extends ParsedRequest

object RequestDeposit {
  implicit val codec: Codec[RequestDeposit] = deriveCodec
}

case class RequestBalance(
    requestId: String,
) extends ParsedRequest

object RequestBalance {
  implicit val codec: Codec[RequestBalance] = deriveCodec
}
