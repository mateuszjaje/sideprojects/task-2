package marlowbanktask

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax._
import io.circe.{Decoder, DecodingFailure, Encoder}

trait AccountEventKafka extends Product

object AccountEventKafka {
  implicit val encoder: Encoder[AccountEventKafka] = Encoder.instance {
    case foo: WithdrawAccountEventKafka => WithdrawAccountEventKafka.encoder.apply(foo)
    case bar: DepositAccountEventKafka  => DepositAccountEventKafka.encoder.apply(bar)
  }

  implicit val decode: Decoder[AccountEventKafka] = Decoder.instance { c =>
    c.get[String]("eventType").flatMap {
      case "withdraw" => c.as[WithdrawAccountEventKafka]
      case "deposit"  => c.as[DepositAccountEventKafka]
      case another    => Left(DecodingFailure(s"unsupported kind $another", c.history))
    }
  }

}

case class WithdrawAccountEventKafka(
    accountId: AccountId,
    amount: Money,
) extends AccountEventKafka

object WithdrawAccountEventKafka {
  private val typeHint = Map("eventType" -> "withdraw").asJson

  val rawEncoder: Encoder[WithdrawAccountEventKafka] = deriveEncoder
  implicit val encoder: Encoder[WithdrawAccountEventKafka] =
    Encoder.instance[WithdrawAccountEventKafka](_.asJson(rawEncoder).deepMerge(typeHint))

  implicit val decoder: Decoder[WithdrawAccountEventKafka] = deriveDecoder
}

case class DepositAccountEventKafka(
    accountId: AccountId,
    amount: Money,
) extends AccountEventKafka

object DepositAccountEventKafka {
  private val typeHint = Map("eventType" -> "deposit").asJson

  val rawEncoder: Encoder[DepositAccountEventKafka] = deriveEncoder
  implicit val encoder: Encoder[DepositAccountEventKafka] =
    Encoder.instance[DepositAccountEventKafka](_.asJson(rawEncoder).deepMerge(typeHint))

  implicit val decoder: Decoder[DepositAccountEventKafka] = deriveDecoder
}
