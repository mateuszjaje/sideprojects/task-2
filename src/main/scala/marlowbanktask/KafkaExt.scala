package marlowbanktask

// example of mapping between models (kafka-domain), done once to present,
// but common in case if I would use avro
// here it seems to be little artificial

object KafkaExt {
  implicit class KafkaToDomainMapper(kafkaModel: AccountEventKafka) {
    def toDomainModel = kafkaModel match {
      case DepositAccountEventKafka(accountId, amount) =>
        Deposit(accountId, amount)
      case WithdrawAccountEventKafka(accountId, amount) =>
        Withdraw(accountId, amount)
    }

  }

  implicit class DomainToKafkaMapper(domainModel: AccountEvent) {
    def toKafkaModel = domainModel match {
      case Deposit(accountId, amount) =>
        DepositAccountEventKafka(accountId, amount)
      case Withdraw(accountId, amount) =>
        WithdrawAccountEventKafka(accountId, amount)
    }

  }

}
