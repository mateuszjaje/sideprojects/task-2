package scraps

import sttp.tapir.server.ziohttp.ZioHttpInterpreter
import sttp.tapir.ztapir._
import zhttp.service.Server
import zio.{Ref, ZIO, ZLayer}

import java.time.ZonedDateTime

object ZIOHttpTarpirServer extends zio.ZIOAppDefault {

  val ref: ZLayer[Any, Nothing, Ref[ResourceView]] = ZLayer.fromZIO(Ref.make(ResourceView("1", 2, ZonedDateTime.now())))

  val retrieveServerEndpoint =
    SimpleEndpoint.retrieveEndpoint
      .zServerLogic(_ => ZIO.service[Ref[ResourceView]].flatMap(_.get))

  val replaceServerEndpoint =
    SimpleEndpoint.createEndpoint
      .zServerLogic { input =>
        val newValue = ResourceView(input._1, input._2.value, input._2.anotherValue)
        ZIO.service[Ref[ResourceView]].flatMap(_.set(newValue).as(newValue))
      }

  val serviceEndpoints: List[ZServerEndpoint[Ref[ResourceView], Any]] = List(
    retrieveServerEndpoint,
    replaceServerEndpoint,
  )

  val swaggerUIEndpoints = SwaggerRoute.swaggerUIZIO.map(_.widen[Ref[ResourceView]])

  val api = ZioHttpInterpreter().toHttp(serviceEndpoints ++ swaggerUIEndpoints)

  override val run = Server.start(8080, api).exitCode.provideLayer(ref)
}
