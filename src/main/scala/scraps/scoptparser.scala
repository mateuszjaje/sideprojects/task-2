package scraps

import scopt.OParser
import zio.{IO, ZIO}

import java.io.File

case class AppCliConfiguration(
    input: Option[File] = None,
    dryRun: Boolean = false,
)

object AppCliConfigurationParser {

  val builder = OParser.builder[AppCliConfiguration]
  val parser = {
    import builder._
    OParser.sequence(
      head("this bot", "1.0.0"),
      opt[File]("input-data")
        .action((x, c) => c.copy(input = Some(x)))
        .required()
        .text("duration how long should it work"),
      opt[Unit]("dry-run")
        .action((_, c) => c.copy(dryRun = true))
        .text("some flag"),
    )
  }

  def parse(args: Seq[String]): Either[Throwable, AppCliConfiguration] =
    OParser.parse(parser, args, AppCliConfiguration()).toRight(new IllegalArgumentException("incorrect data"))

  def parseZio(args: Seq[String]): IO[IllegalArgumentException, AppCliConfiguration] =
    ZIO.fromEither(OParser.parse(parser, args, AppCliConfiguration()).toRight(new IllegalArgumentException("incorrect data")))

}
