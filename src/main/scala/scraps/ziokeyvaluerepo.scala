package scraps

import better.files.File
import cats.syntax.either._
import cats.syntax.option._
import io.circe.syntax.EncoderOps
import io.circe.{jawn, Codec, Printer}
import zio.{Ref, Tag, Task, ULayer, ZIO, ZLayer}

import java.math.BigInteger
import java.nio.file.StandardOpenOption
import java.security.MessageDigest

trait EntityKey[T] {
  def key: T
}

trait SomeCrudRepo[KeyType, EntityType <: EntityKey[KeyType]] {
  def put(entity: EntityType): Task[Unit]

  def find(key: KeyType): Task[Option[EntityType]]

  def getAll: Task[Vector[EntityType]]

  def updateEntity(key: KeyType, updateFn: EntityType => EntityType): Task[Option[EntityType]]
}

class ZioRefCrudRepo[KeyType, EntityType <: EntityKey[KeyType]](
    ref: Ref.Synchronized[Map[KeyType, EntityType]],
) extends SomeCrudRepo[KeyType, EntityType] {
  override def put(entity: EntityType): Task[Unit] = ref.update(_.updated(entity.key, entity)).unit

  override def find(key: KeyType): Task[Option[EntityType]] = ref.get.map(_.get(key))

  override def getAll: Task[Vector[EntityType]] = ref.get.map(_.values.toVector)

  override def updateEntity(key: KeyType, updateFn: EntityType => EntityType): Task[Option[EntityType]] = {
    ref.modifyZIO { data =>
      data
        .get(key)
        .map { existing =>
          val updatedEntity = updateFn(existing)
          if (updatedEntity.key != existing.key) ZIO.fail(new IllegalArgumentException("updateFn cannot change entity key"))
          else ZIO.succeed(Some(existing) -> data.updated(existing.key, updatedEntity))
        }
        .getOrElse(ZIO.succeed(None -> data))
    }
  }

}

object ZioRefCrudRepo {
  def make[KeyType: Tag, EntityType <: EntityKey[KeyType]: Tag]: ULayer[ZioRefCrudRepo[KeyType, EntityType]] =
    ZLayer.fromZIO(Ref.Synchronized.make(Map[KeyType, EntityType]()).map(new ZioRefCrudRepo(_)))

}

class JsonFileBasedCrudRepo[KeyType, EntityType <: EntityKey[KeyType]: Codec](parentDir: File) extends SomeCrudRepo[KeyType, EntityType] {
  private def hashKey(key: String) = ZIO.fromEither(Either.catchNonFatal {
    val hasher = MessageDigest.getInstance("MD5")
    hasher.update(key.getBytes)
    val bigInt = new BigInteger(1, hasher.digest())
    bigInt.toString(16).toUpperCase
  })

  def fileOfADoc(key: String) =
    hashKey(key).map(_ + ".json").map(parentDir./)

  override def find(key: KeyType) = {
    fileOfADoc(key.toString)
      .flatMap { dataFile =>
        if (dataFile.isRegularFile)
          ZIO.fromEither(jawn.decode[EntityType](dataFile.contentAsString)).map(_.some)
        else ZIO.none
      }
  }

  private def persist(value: EntityType, file: File) = {
    ZIO
      .fromEither(Either.catchNonFatal {
        file.createFileIfNotExists()
        file.overwrite(value.asJson.printWith(Printer.spaces2)) //(openOptions = Seq(StandardOpenOption.WRITE))
      })
      .unit
  }

  override def put(value: EntityType) = {
    fileOfADoc(value.key.toString)
      .flatMap { dataFile =>
        if (dataFile.isRegularFile)
          ZIO.fail(new IllegalStateException(s"entity ${value.key} already exists"))
        else
          persist(value, dataFile)
      }
  }

  override def getAll =
    ZIO.foreach(parentDir.children.toVector)(f => ZIO.fromEither(jawn.decode[EntityType](f.contentAsString)))

  override def updateEntity(key: KeyType, updateFn: EntityType => EntityType) = {
    fileOfADoc(key.toString)
      .flatMap { dataFile =>
        if (dataFile.isRegularFile)
          for {
            current <- ZIO.fromEither(jawn.decode[EntityType](dataFile.contentAsString))
            updatedEntity = updateFn(current)
            _ <- ZIO.cond(updatedEntity.key == current.key, (), new IllegalArgumentException("updateFn cannot change entity key"))
            _ <- persist(updatedEntity, dataFile)
          } yield Some(current)
        else ZIO.none
      }
  }

}
