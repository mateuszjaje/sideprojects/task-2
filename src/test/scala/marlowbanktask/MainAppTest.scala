package marlowbanktask

import com.goyeau.kafka.streams.circe.CirceSerdes._
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.kstream.GlobalKTable
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.kstream.Consumed
import org.apache.kafka.streams.{StreamsConfig, TopologyTestDriver}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.apache.kafka.streams.scala.ImplicitConversions._

import java.util.Properties
import scala.jdk.CollectionConverters.ListHasAsScala
import scala.util.Random

class MainAppTest extends AnyFlatSpec with Matchers {

  behavior of "The app"

  // requires more configurability, maybe configurable topics per topology, so one test driver can handle all tests with proper isolation
  // now I generate another applicationID to ensure isolation
  // or assertions could work with non-isolated env, e.g. filtering messages to assert by key and just use another account in each testcase
  // for task like this one the simplest test context is enough
  trait Ctx {
    val config = new Properties()
    config.put(StreamsConfig.APPLICATION_ID_CONFIG, Random.alphanumeric.take(40).mkString)
    config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234")
    val streamsBuilder = new StreamsBuilder()
    import MySerdes.accountIdSerde

    val readModelGT: GlobalKTable[AccountId, AccountAgg] = streamsBuilder.globalTable[AccountId, AccountAgg](Topics.ReadModel)

    MainApp.attachMainAppTopology(readModelGT, streamsBuilder)
    Notifier.attachNotifierTopology(readModelGT, streamsBuilder)
    val t  = streamsBuilder.build()
    val td = new TopologyTestDriver(t, config)

    val requestsTopic  = td.createInputTopic(Topics.Requests, Serdes.String().serializer(), Serdes.String().serializer())
    val responsesTopic = td.createOutputTopic(Topics.Responses, Serdes.String().deserializer(), Serdes.String().deserializer())
    val eventsStore    = td.createOutputTopic(Topics.Events, Serdes.String().deserializer(), Serdes.String().deserializer())
    val readModel      = td.createOutputTopic(Topics.ReadModel, Serdes.String().deserializer(), Serdes.String().deserializer())
    val notifications  = td.createOutputTopic(Topics.Notifications, Serdes.String().deserializer(), Serdes.String().deserializer())
  }

  it should "respond for unparseable request" in new Ctx {
    requestsTopic.pipeInput("acc-1", """{"requestId": "req-1", "kind": "depdsosit", "amount":100}""")
    val response = responsesTopic.readRecord()
    response.key() shouldBe "acc-1"
    response.value() shouldBe """{"requestId":"req-1","response":"unknown case Request(req-1,depdsosit,Some(100))"}"""
    td.close()
  }
  it should "perform and respond to debit request" in new Ctx {
    requestsTopic.pipeInput("acc-2", """{"requestId": "req-1", "kind": "deposit", "amount":100}""")
    val eventsStoreRecord = eventsStore.readRecord()
    eventsStoreRecord.key() shouldBe "acc-2"
    eventsStoreRecord.value() shouldBe """{"eventType":"deposit","accountId":"acc-2","amount":100}"""
    val responseRecord = responsesTopic.readRecord()
    responseRecord.key() shouldBe "acc-2"
    responseRecord
      .value() shouldBe """{"requestId":"req-1","response":"event accepted: DepositAccountEventKafka(AccountId(acc-2),Money(100))"}"""
    val readModelData = readModel.readRecordsToList().asScala
    readModelData should have size 1
    readModelData.head.value() shouldBe """{"id":"acc-2","balance":100}"""
    td.close()
  }
  it should "perform and respond to balance request" in new Ctx {
    requestsTopic.pipeInput("acc-3", """{"requestId": "req-1", "kind": "deposit", "amount":100}""")
    eventsStore.readRecord()
    requestsTopic.pipeInput("acc-3", """{"requestId": "req-2", "kind": "balance"}""")
    responsesTopic.readRecord()
    val responseRecord = responsesTopic.readRecord()
    responseRecord.key() shouldBe "acc-3"
    responseRecord
      .value() shouldBe """{"requestId":"req-2","response":"Your balance is Money(100)"}"""
    td.close()
  }
  it should "perform and respond to withdraw request if correct" in new Ctx {
    requestsTopic.pipeInput("acc-4", """{"requestId": "req-1", "kind": "deposit", "amount":100}""")
    requestsTopic.pipeInput("acc-4", """{"requestId": "req-2", "kind": "withdraw", "amount": 60}""")
    val responses = responsesTopic.readValuesToList().asScala.toList
    responses should have size 2
    responses.last shouldBe """{"requestId":"req-2","response":"event accepted: WithdrawAccountEventKafka(AccountId(acc-4),Money(60))"}"""
    val events = eventsStore.readRecordsToList().asScala
    events should have size 2
    events.last.key() shouldBe "acc-4"
    events.last.value() shouldBe """{"eventType":"withdraw","accountId":"acc-4","amount":60}"""
    val readModelData = readModel.readRecordsToList().asScala
    readModelData should have size 2
    readModelData.head.value() shouldBe """{"id":"acc-4","balance":100}"""
    readModelData.last.value() shouldBe """{"id":"acc-4","balance":40}"""
  }
  it should "perform and respond to withdraw request if incorrect" in new Ctx {
    requestsTopic.pipeInput("acc-5", """{"requestId": "req-1", "kind": "deposit", "amount":100}""")
    requestsTopic.pipeInput("acc-5", """{"requestId": "req-2", "kind": "withdraw", "amount": 110}""")
    val responses = responsesTopic.readValuesToList().asScala.toList
    responses should have size 2
    responses.last shouldBe """{"requestId":"req-2","response":"CantWithdraw(balance too low)"}"""
    val events = eventsStore.readRecordsToList().asScala
    events should have size 1
    val readModelData = readModel.readRecordsToList().asScala
    readModelData should have size 1
    readModelData.head.value() shouldBe """{"id":"acc-5","balance":100}"""
  }
  it should "stack deposits in the read model" in new Ctx {
    requestsTopic.pipeInput("acc-6", """{"requestId": "req-1", "kind": "deposit", "amount":100}""")
    requestsTopic.pipeInput("acc-6", """{"requestId": "req-2", "kind": "deposit", "amount":60}""")

    val readModelData = readModel.readRecordsToList().asScala
    readModelData should have size 2
    readModelData.head.value() shouldBe """{"id":"acc-6","balance":100}"""
    readModelData.last.value() shouldBe """{"id":"acc-6","balance":160}"""
    td.close()
  }
  it should "trigger notification when expected" in new Ctx {
    requestsTopic.pipeInput("acc-7", """{"requestId": "req-1", "kind": "deposit", "amount":11000}""")
    requestsTopic.pipeInput("acc-7", """{"requestId": "req-2", "kind": "withdraw", "amount":2000}""")

    val notificationEnties = notifications.readRecordsToList().asScala
    notificationEnties should have size 1
    notificationEnties.head.value() shouldBe """{"acc":"acc-7","currentBalance":9000}"""
    td.close()
  }
  it should "not trigger notification when not expected" in new Ctx {
    requestsTopic.pipeInput("acc-8", """{"requestId": "req-1", "kind": "deposit", "amount":9000}""")
    requestsTopic.pipeInput("acc-8", """{"requestId": "req-2", "kind": "withdraw", "amount":2000}""")

    val notificationEnties = notifications.readRecordsToList().asScala
    notificationEnties should have size 0
    td.close()
  }

}
