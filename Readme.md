
# Summary

* two main apps implemented, TheApp and Notifier, responsible for the requirements
* demo deployment done as a docker compose file: zookeeper + kafka + topics creation + kiwi-kafka + two app containers
* kiwi accessible at localhost:8090
* simultaneous access not really implemented, I left a note in the code about that
* topic used to notify about events is the event storage at the same time - kafka as a database - controversial approach
but for this task purpose should be enough
* dockerized solution done - with sbt-native-packager, 99% work done out of the box, just type sbt Docker/publishLocal
* the solution is not 100% concurrent safe as it uses "read-model" to validate withdraw action
so there is always some short delay between event validation / creation and read-model update before next event, to make it 100% safe it should be implemented
as read all the events (from kafka topic) to create aggreagate and decide based on that, due to limited time I decided to go
easier way and mention it here as it was a decision not a gap
* logging - I would implement it with logback and scala-logging, so no rocket science, there are some printlns, but I think
it is not what you guys wanted
* money expressed as number of cents to simplify implementation (otherwise I would go with BigDecimal to overcome usual issues
with roundings in trivial floating points numeric types)
* all topics are partitioned by account-id, it is also a simplification to keep one account actions executed one by one,
it could be improved by decoupling balance checks to another topic, which could be partitioned by whatever and keep balance altering actions
still partitioned by account-id
* stress testing - well, if the 100% safety would be fixed I would do a test of withdraw scenario continuously sending deposits and withdraws
trying to exploit withdraw validation, e.g. deposit + two withdraws (for more than the deposit amount) as a round + worth to check if 
access to the account is indeed concurrent, but this is also not implemented as I also mentioned above (balance requests
are processed in sequence per account)

### note
this is my very first time with kafka-streams, nice library. however not knowing all the concepts I could make critical mistakes in the implementation.

I could implement things, which I described above, but on the other hand I have limited time, so I had to stop somewhere and just describe
next steps.


```shell
sbt Docker/publishLocal
cd app-compose
docker-compose up
```