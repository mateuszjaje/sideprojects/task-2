import com.typesafe.sbt.packager.docker.ExecCmd

organization := "io.gitlab.mateuszjaje"
scalaVersion := "2.13.10"
name         := "themarlowtask"
version      := "1.0.0"

val zioVersion    = "2.+"
val tapirVersion  = "1.1.+"
val sttpVersion   = "3.8.+"
val Http4sVersion = "0.23.+"
val kindProjector = "0.13.2"

enablePlugins(JavaAppPackaging, AshScriptPlugin)

dockerCommands := dockerCommands.value.filter { // drop entrypoint & cmd overrides
  case ExecCmd("ENTRYPOINT", _*) | ExecCmd("CMD", _*) => false
  case _                                              => true
} :+ ExecCmd("CMD", "echo", "This is a multi app image, please specify what app you want to run")

libraryDependencies ++= Seq(
  "org.apache.kafka"  % "kafka-streams"            % "3.3.1",
  "org.apache.kafka"  % "kafka-streams-test-utils" % "3.3.1",
  "org.apache.kafka" %% "kafka-streams-scala"      % "3.3.1",
  "com.goyeau"       %% "kafka-streams-circe"      % "0.6.3" excludeAll (
    "io.circe"       %% "circe-parser",
    "io.circe"       %% "circe-core",
    "io.circe"       %% "circe-generic",
  ),
  "com.github.pathikrit"          %% "better-files"             % "3.+",
  "org.typelevel"                 %% "cats-core"                % "2.8.+",
  "com.github.pureconfig"         %% "pureconfig"               % "0.17.+",
  "com.github.scopt"              %% "scopt"                    % "4.1.+",
  "org.scalatest"                 %% "scalatest"                % "3.2.+",
  "com.softwaremill.sttp.client3" %% "core"                     % sttpVersion,
  "com.softwaremill.sttp.client3" %% "zio"                      % sttpVersion,
  "com.softwaremill.sttp.client3" %% "circe"                    % sttpVersion,
  "dev.zio"                       %% "zio"                      % zioVersion,
  "dev.zio"                       %% "zio-test"                 % zioVersion % Test,
  "dev.zio"                       %% "zio-test-sbt"             % zioVersion % Test,
  "dev.zio"                       %% "zio-test-magnolia"        % zioVersion % Test,
  "com.softwaremill.sttp.tapir"   %% "tapir-zio"                % tapirVersion,
  "com.softwaremill.sttp.tapir"   %% "tapir-json-circe"         % tapirVersion,
  "com.softwaremill.sttp.tapir"   %% "tapir-sttp-client"        % tapirVersion,
  "com.softwaremill.sttp.tapir"   %% "tapir-sttp-stub-server"   % tapirVersion,
  "com.softwaremill.sttp.tapir"   %% "tapir-zio-http-server"    % tapirVersion,
  "com.softwaremill.sttp.tapir"   %% "tapir-http4s-server"      % tapirVersion,
  "com.softwaremill.sttp.tapir"   %% "tapir-http4s-server-zio"  % tapirVersion,
  "com.softwaremill.sttp.tapir"   %% "tapir-swagger-ui-bundle"  % tapirVersion,
  "com.softwaremill.sttp.tapir"   %% "tapir-openapi-circe-yaml" % "1.0.0-M9",
  "org.http4s"                    %% "http4s-blaze-server"      % Http4sVersion,
)

addCompilerPlugin("org.typelevel" %% "kind-projector" % kindProjector cross CrossVersion.full)

testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")
